'use strict';
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors');
const app = express();
app.use(cors());
const multer = require('multer');
fs = require('fs-extra')
app.use(bodyParser.urlencoded({ extended: true }))

const MongoClient = require('mongodb').MongoClient
ObjectId = require('mongodb').ObjectId

// const myurl = 'mongodb://localhost:27017';

const AWS = require('aws-sdk');
AWS.config.update({ region: 'ap-southeast-1' });

app.listen(5000, () => {
  console.log('listening on 5000')
})

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/")
  },
  filename: function (req, file, cb) {
    // console.log(file);
    cb(null, file.originalname)
  }
})

var upload = multer({ storage: storage })

let bucketName = "qualee-document-data";
var accessKey = "AKIAYB6EZBVQ4EQCL4HL";
var secretKey = "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";

headers = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Credentials": true
}

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');

});

// upload single file

app.post('/uploadfile', upload.single("myFile"), (req, res, next) => {
  let bucketName = "qualee-document-data";
  var accessKey = "AKIAYB6EZBVQ4EQCL4HL";
  var secretKey = "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";
  var file = req.file;
  var filepath = req.file.path;
  console.log(filepath);
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    accessKeyId: accessKey,
    secretAccessKey: secretKey,
  });
  var uploadParams = { Bucket: bucketName, Key: '', Body: '' };
  var fs = require('fs');
  var fileStream = fs.createReadStream(filepath);
  fileStream.on('error', function (err) {
    console.log('File Error', err);
  });
  uploadParams.Body = fileStream;
  var path = require('path');
  uploadParams.Key = path.basename(filepath);
  s3.upload(uploadParams, async function (err, data) {
    if (err) {
      console.log("Error", err);
    } if (data) {
      console.log("Upload Success", data.Key);
    }
  });
  res.headers("Access-Control-Allow-Origin", "*");
  res.json({
    success: true,
    msg: 'This is CORS-enabled for all origins!'
  })
  // res.send(file);
})

app.post('/updatefile', upload.single("myFile"), (req, res, next) => {
  console.log("Desired filename to update: " + req.body.filename);
  var file = req.file;
  var filepath = req.file.path;
  console.log("Updated filename by user: " + filepath);
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    accessKeyId: accessKey,
    secretAccessKey: secretKey,
  });

  // Delete the old one first:
  var deletingParams = { Bucket: bucketName, Key: req.body.filename };
  s3.deleteObject(deletingParams, function (err, data) {
    if (err) {
      console.log(err, err.stack);
      res.send(err);
      return;
    }
    else {
      console.log(data);
    }
  });

  // Upload the new one:
  var uploadParams = { Bucket: bucketName, Key: '', Body: '', ACL: 'public-read', };
  var fs = require('fs');
  var fileStream = fs.createReadStream(filepath);
  fileStream.on('error', function (err) {
    console.log('File Error', err);
  });
  uploadParams.Body = fileStream;
  var path = require('path');
  uploadParams.Key = req.body.filename;
  s3.upload(uploadParams, async function (err, data) {
    if (err) {
      console.log("Error", err);
    } if (data) {
      console.log("Upload Success", data.Key);
    }
  });
  res.set(headers);
  res.send(file);
})

//Uploading multiple files
app.post('/uploadmultiple', upload.array('myFiles', 12), (req, res, next) => {
  const files = req.files
  if (!files) {
    const error = new Error('Please choose files')
    error.httpStatusCode = 400
    return next(error)
  }
  res.send(files);
})


app.post('/uploadphoto', upload.single('picture'), (req, res) => {
  var img = fs.readFileSync(req.file.path);
  var encode_image = img.toString('base64');
  // Define a JSONobject for the image attributes for saving to database

  var finalImg = {
    contentType: req.file.mimetype,
    image: new Buffer(encode_image, 'base64')
  };
  db.collection('mycollection').insertOne(finalImg, (err, result) => {
    console.log(result);
    if (err) return console.log(err)
    console.log('saved to database')
    res.redirect('/');
  })
})


app.get('/photos', (req, res) => {
  db.collection('mycollection').find().toArray((err, result) => {
    const imgArray = result.map(element => element._id);
    console.log(imgArray);
    if (err) return console.log(err)
    res.send(imgArray);
  });
});

app.get('/photo/:id', (req, res) => {
  var filename = req.params.id;
  db.collection('mycollection').findOne({ '_id': ObjectId(filename) }, (err, result) => {
    if (err) return console.log(err)
    res.contentType('image/jpeg');
    res.send(result.image.buffer)


  })
})

module.exports.handler = serverless(app);
